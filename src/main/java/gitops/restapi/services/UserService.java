package gitops.restapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import gitops.restapi.models.db.User;
import gitops.restapi.models.repositories.UserRepository;

@Service
public class UserService {

    private final UserRepository repo;

    @Autowired
    public UserService(
        UserRepository repo
    ) {
        this.repo = repo;
    }

    @Transactional(readOnly = true)
    public List<User> list() {
      return repo.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<User> findById(Long id) {
        return repo.findById(id);
    }

    @Transactional
    public User create(User template) {
        return repo.saveAndFlush(template);
    }

    @Transactional
    public Optional<User> update(Long id, User updated) {
        return repo.findById(id).map((user) -> {
            user = updateWith(user, updated);
            return repo.saveAndFlush(user);
        });
    }

    @Transactional
    public void delete(Long id) {
        repo.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> findOldest() {
        return repo.findOldest();
    }

    @Transactional(readOnly = true)
    public Optional<User> findYoungest() {
        return repo.findYoungest();
    }

    private User updateWith(User original, User update) {
        original.setFirstName(update.getFirstName());
        original.setLastName(update.getLastName());
        original.setEmail(update.getEmail());
        original.setAge(update.getAge());
        return original;
    }

    
}
